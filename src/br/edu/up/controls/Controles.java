package br.edu.up.controls;

import br.edu.up.models.Ano;
import br.edu.up.models.Mes;
import br.edu.up.models.Prompt;

public class Controles {

    public void criarAgenda(){

        Prompt.imprimir("---------------------- iniciando Agenda ----------------------");

        

        Ano ano = new Ano(2024,false);
        Mes mes = new Mes();

        mes.imprimirMes();

        String definirPessoa = Prompt.lerLinha("digite o nome da pessoa para o compromisso: ");
        String definirLocal = Prompt.lerLinha("digite o local para o compromisso: ");
        String definirAssunto = Prompt.lerLinha("digite o assunto para o compromisso: ");
        Integer definirHora = Prompt.lerInteiro("digite a hora do compromisso: ");
        Integer definirDia = Prompt.lerInteiro("digite o dia do mes do compromisso: ");
        Integer definirMes = Prompt.lerInteiro("digite o numero do mes do compromisso: ");

        mes.adicionarCompromisso(definirPessoa, definirLocal, definirAssunto, definirHora);
        mes.adicionarCompromissoDia(definirDia, definirMes);
        mes.listarCompromissos(definirMes);
        ano.StatusAno();

        mes.excluirCompromisso();
        mes.excluirCompromissoMes(definirMes);

        mes.listarCompromissos(definirMes);
        ano.StatusAno();

        mes.redefinirCompromissos();
        mes.listarCompromissos(definirMes);
        ano.StatusAno();

    }

}
