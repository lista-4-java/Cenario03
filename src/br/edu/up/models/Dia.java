package br.edu.up.models;

public class Dia {
    private Integer diaMes;
    private Compromisso[] compromissos = new Compromisso[1];

    public Dia(Integer diaMes) {
        this.diaMes = diaMes;
    }

    public Dia() {
    
    }

    public Integer getDiaMes() {
        return diaMes;
    }

    public void setDiaMes(Integer diaMes) {
        this.diaMes = diaMes;
    }

    // ----------------------------------------------------------------

    public void adicionarCompromisso(String definirPessoa, String definirLocal, String definirAssunto, Integer definirHora) {

        compromissos[0] = new Compromisso(definirPessoa, definirLocal, definirAssunto, definirHora);

    }

    public void consultarCompromisso() {

            Prompt.imprimir("");
            Prompt.imprimir("---------------- CONSULTAR HORA ------------------------------------");
            Prompt.imprimir("");
            Prompt.imprimir("a hora do seu compro misso e: " + compromissos[0].toString());
            Prompt.imprimir("");

    }

    public void excluirCompromisso() {

        compromissos[0].setHora(null);
        compromissos[0].setAssunto(null);
        compromissos[0].setLocal(null);
        compromissos[0].setPessoa(null);

        Prompt.imprimir("-----------------------Compromissso Excluido com sucesso -----------------------------");

    }

    public void listarCompromissos() {

                Prompt.imprimir("");
                Prompt.imprimir("---------------- CONSULTAR COMPROMISSO ------------------------------------");
                Prompt.imprimir("");
                Prompt.imprimir("seu compromisso e:" + compromissos[0].toString());
                Prompt.imprimir("");
    }
}

