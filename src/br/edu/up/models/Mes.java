package br.edu.up.models;

public class Mes {

    private String nomeMes[] = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto","Setembro", "Outubro", "Novembro", "Dezembro" };
    private int qtdeDias[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    private Integer definirMes;
    private String auxNomeMes[] = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto","Setembro", "Outubro", "Novembro", "Dezembro" };

    private Dia[] dias = new Dia[1];
    private Compromisso[] compromissos = new Compromisso[1];

    public Mes() {

    }

    public void verificarAnoBissexto(boolean bissexto) {
        if (bissexto == true) {
            qtdeDias[1] = 29;
        } else {
            qtdeDias[1] = 28;
        }
    }

    public void imprimirMes() {

        Prompt.imprimir("");
        Prompt.imprimir(
                "--------------------INFORMATIVO // MESES E QUANTOS DIAS POSSUI O MES ---------------------------");
        Prompt.imprimir("");

        for (int i = 0; i < nomeMes.length; i++) {
            Prompt.imprimir(nomeMes[i] + " " + "(" + qtdeDias[i] + ")");
        }

        Prompt.imprimir(
                "-------------- NA HORA DE DEFINIR O MES LEMBRAR QUE O MES DE JANEIRO E O MES 0 E DEZEMBRO O MES 11 ---------");
        Prompt.imprimir("");
    }

    public void verMes(Integer definirMes) {

        Prompt.imprimir("voce escolheu o mes de: " + nomeMes[definirMes]);
    }


    public void adicionarCompromissoDia(int definirDia, int definirMes) {
        if (definirDia > qtdeDias[definirMes]) {
            Prompt.imprimir("O dia que você digitou não existe.");

            definirDia = Prompt.lerInteiro("Digite o dia do compromisso: ");
            dias[0] = new Dia();
            dias[0].setDiaMes(definirDia);

            adicionarCompromissoDia(definirDia, definirMes);
        } else {
            dias[0] = new Dia();
            dias[0].setDiaMes(definirDia);
            Prompt.imprimir("Voce escolheu o dia: " + dias[0].getDiaMes());
        }
    }

    public void adicionarCompromisso(String definirPessoa, String definirLocal, String definirAssunto,Integer definirHora) {

        compromissos[0] = new Compromisso(definirPessoa, definirLocal, definirAssunto, definirHora);

        Prompt.imprimir("---------------------- ADICIONADOS OS DADOS DOS COMPROMISSO ------------------------------------------");
    }

    public void redefinirCompromissos() {

        for (int i = 0; i < auxNomeMes.length; i++) {

            this.nomeMes[i] = auxNomeMes[i];

        }

        String definirPessoa = Prompt.lerLinha("digite o nome da pessoa para o compromisso: ");
        String definirLocal = Prompt.lerLinha("digite o local para o compromisso: ");
        String definirAssunto = Prompt.lerLinha("digite o assunto para o compromisso: ");
        Integer definirHora = Prompt.lerInteiro("digite a hora do compromisso: ");
        Integer definirDia = Prompt.lerInteiro("digite o dia do mes do compromisso: ");
        Integer definirMes = Prompt.lerInteiro("digite o numero do mes do compromisso: ");
          
        adicionarCompromisso(definirPessoa, definirLocal, definirAssunto, definirHora);
        adicionarCompromissoDia(definirDia, definirMes);
    }

    public void excluirCompromisso() {

        dias[0].setDiaMes(null);
        compromissos[0].setHora(null);
        compromissos[0].setAssunto(null);
        compromissos[0].setLocal(null);
        compromissos[0].setPessoa(null);
    

        Prompt.imprimir("----------------- HORA / DIA EXCLUIDO COM SUCESSO --------------------");
    }

    public void excluirCompromissoMes(Integer definirMes) {

        for (int i = 0; i < auxNomeMes.length; i++) {

            this.nomeMes[i] = null;

        }
            
        
        Prompt.imprimir("----------------- MES EXCLUIDO COM SUCESSO --------------------");
    }

    public void listarCompromissos(Integer definirMes) {

        Prompt.imprimir("------------------------- SEU COMPROMISSO ------------------------------------------");

        Prompt.imprimir("o dia do seu compromisso e: " + dias[0].getDiaMes());

        Prompt.imprimir("o mes do seu compromisso e: " + nomeMes[definirMes]);

        Prompt.imprimir(compromissos[0].toString());

    }
}
